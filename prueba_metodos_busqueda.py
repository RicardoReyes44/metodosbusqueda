'''
Created on 4 dic. 2020

@author: RSSpe
'''

from typing import List


class FuncionesHash:
    def __init__(self):
        self.tam = 20
        self.indices = [None] * self.tam
        self.datos = [None] * self.tam

    def agregar(self,clave,dato):
        valorHash = self.funcionHash(clave,len(self.indices))
        if self.indices[valorHash] == None:
            self.indices[valorHash] = clave
            self.datos[valorHash] = dato
        else:
            if self.indices[valorHash] == clave:
                self.datos[valorHash] = dato
            else:
                proximoIndice = self.rehash(valorHash,len(self.indices))
                while self.indices[proximoIndice] != None and \
                        self.indices[proximoIndice] != clave:
                    proximoIndice = self.rehash(proximoIndice,len(self.indices))
                    if self.indices[proximoIndice] == None:
                        self.indices[proximoIndice]=clave
                        self.datos[proximoIndice]=dato
                    else:
                        self.datos[proximoIndice] = dato

    def funcionHash(self,clave,tam):
        return clave%tam

    def rehash(self,hashViejo,tam):
        return (hashViejo+1)%tam

    def obtener(self,clave):
        indiceInicio = self.funcionHash(clave,len(self.indices))
        dato = None
        parar = False
        encontrado = False
        posicion = indiceInicio
        while self.indices[posicion] != None and  \
                not encontrado and not parar:
            if self.indices[posicion] == clave:
                encontrado = True
                dato = self.datos[posicion]
            else:
                posicion=self.rehash(posicion,len(self.indices))
                if posicion == indiceInicio:
                    parar = True
                    return dato

    def __getitem__(self,clave):
        if clave in self.indices:
            
            i=0
            
            while i<len(self.indices):
                if clave==self.indices[i]:
                    break
                i+=1
            
            return self.datos[i]
    
    def imprimir(self):
        print("Indices: ", self.indices)
        print("Datos: ", self.datos)

    def __setitem__(self,clave,dato):
        self.agregar(clave,dato)

class BinarySearch:
    
    def binarySearch(self, array, buscado, primero, ultimo):

        if primero>ultimo:
            return False
        else:
            
            mitad = (primero+ultimo)//2
            
            if buscado==array[mitad]:
                return True
            elif buscado<array[mitad]:
                return self.binarySearch(array, buscado, primero, mitad-1)
            else:
                return self.binarySearch(array, buscado, mitad+1, ultimo)


mb = BinarySearch()
funcion=FuncionesHash()
array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]

i=0

while i<len(array):
    funcion.agregar(array[i], array[i]+10)
    i+=1

while(True):
    
    print("---------------MENU----------------")
    print("1.- Busqueda binaria")
    print("2.- Busqueda Hash")
    print("3.- Salir")
    
    try:
        opcion = int(input("Introduce una opcion: "))
    
        if opcion==1:
        
            numero = int(input("Introduce un numero: "))
            print(f"\n>>> {mb.binarySearch(array, numero, 0, len(array)-1)}")

        elif opcion==2:
        
            numero = int(input("Introduce un numero: "))
            print(f"\nInformacion >>> {funcion.__getitem__(numero)}")
            
            funcion.imprimir()

        elif opcion==3:

            print("\n\n-------------------\nPrograma terminado\n-------------------")
            break

        else:
            print("Opcion invalida, prueba de nuevo")
    except ValueError as error:
        print(f"Error en la entrada de datos: {error}, vuelve a intentarlo")
    print()
